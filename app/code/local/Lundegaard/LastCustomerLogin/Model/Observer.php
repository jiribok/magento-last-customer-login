<?php
/**
 * Created by PhpStorm.
 * User: jiri.bok
 * Date: 10/19/15
 * Time: 1:46 PM
 */

class Lundegaard_LastCustomerLogin_Model_Observer extends Varien_Event_Observer
{
    /**
     * Adds column to admin customers grid
     *
     * @param Varien_Event_Observer $observer
     * @return Lundegaard_LastCustomerLogin_Model_Observer
     */
    public function appendCustomColumn(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if (!isset($block)) {
            return $this;
        }

        if ($block->getType() == 'adminhtml/customer_grid') {
            /* @var $block Mage_Adminhtml_Block_Customer_Grid */
            $block->addColumnAfter('last_login', array(
                'header'    => Mage::helper("lastcustomerlogin")->__('Last Login'),
                'type'      => 'datetime',
                'index'     => 'last_login',
                'align'     => 'center'
            ), 'customer_since');
        }
    }

    /**
     * Catch the customer collection before load
     *
     * @param Varien_Event_Observer $observer
     */
    public function beforeCollectionLoad(Varien_Event_Observer $observer)
    {
        $collection = $observer->getCollection();
        if (!isset($collection)) {
            return;
        }

        /**
         * Mage_Customer_Model_Resource_Customer_Collection
         */
        if ($collection instanceof Mage_Customer_Model_Resource_Customer_Collection) {
            /* @var $collection Mage_Customer_Model_Resource_Customer_Collection */
            $collection->addAttributeToSelect('last_login');
        }
    }

    /**
     * Catch the customer login event
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerLogin($observer)
    {
        try {
            $customer = $observer->getCustomer();
            $customer->setData('last_login', Mage::getModel('core/date')->date('Y-m-d H:i:s'));
            $customer->getResource()->saveAttribute($customer, 'last_login');
        } catch (Exception $ex) {
            Mage::log("An exception has occurred:\n".var_export($ex, true), Zend_Log::NOTICE, 'last-customer-login.txt', true);
        }
    }
}